import 'package:flutter/material.dart';
import 'package:knowledgebook/screens/home.dart';
import 'package:knowledgebook/screens/searchbook.dart';
import 'package:knowledgebook/screens/bio.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application;
  // Awesome
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData.dark(),
      home: new Home(),
      routes:  <String,WidgetBuilder>{
        'Search' : (BuildContext context)=> new Searchbook(),
        'Bio' : (BuildContext context)=> new Bio()
         },
    );
  }
}

