import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:knowledgebook/dataholder/tempdata.dart';


class Home extends StatefulWidget{
@override
_State createState()=> new _State();
}

class _State extends State<Home>{

  _searchbook(){
    Navigator.of(context).pushNamed('Search');
  }

Future _launchbio(String url) async{
  final temp = new tempdata();
  temp.url=url;
  String _url1 = temp.url;
  Navigator.of(context).pushNamed('Bio');
  
}

  // List of people
  List _data;

  // Fetch and Update Data
  String uri = "http://catchforms.in/knowledgebook/scripts/peoplelist.php";
  Future<String> getData() async {
    var response = await http.get(
      Uri.encodeFull(uri),
      headers: {
        "Accept" : "application/json"
      }
    );

    // print(response.body);
    Map map = JSON.decode(response.body);
    setState((){
      _data = map["list"] as List;
      storelist(_data);
    });
    }

    // Store Data List in Cache
    Future<Null> storelist(List _data) async{
      final SharedPreferences preferences = await SharedPreferences.getInstance();
      //  preferences.clear();
      // print("Log 1 : "+_data.toString());
      preferences.setString("list", _data.toString());
     
     }

    //  Get Data List from Cache
    Future<Null> getlist() async{
      final SharedPreferences preferences = await SharedPreferences.getInstance();
      String value = preferences.getString("list");
  }

  @override
    void initState() {
      // TODO: implement initState
      
      // Get Cache List Data
      // try{
      //      getlist();
      // }catch(e){
      //   print("Error in Cacher Memory");
      // }
      getData();
    }


  @override
  Widget build(BuildContext context){

    return new Scaffold(
      appBar: new AppBar(title: _appbar()),
      body: _body(),
    );
  }

  // App Bar
    Widget _appbar(){

    return new Row(
      children: <Widget>[
        new Flexible(
          flex: 1,
          child: new Text("Home",
          style: new TextStyle(
            fontSize: 20.0
          ),),
        ),

        new Flexible(
          flex: 5,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
             new IconButton(icon: new Icon(Icons.search),onPressed: (){_searchbook();},)
            ],
          )
        )
      ],
    );
  }


Widget _listitems(int index){

Map _indexdata = _data[index];
String _name = _indexdata["name"];
String _quotes = _indexdata["quotes"];
String _image = _indexdata["pictureurl"];
String _biourl= _indexdata["biographyurl"];

  return new Card(
    child: new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Column(
        children: <Widget>[

          new Column(
            children: <Widget>[
              
              new Center(
                child: new Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: new Text(_name,
                  style: new TextStyle(
                    fontSize: 20.0,
                    letterSpacing: 1.0,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.italic
                  ),),
                ),
              ),
          new Center(
          child: new Image.network(_image,
          height: 130.0,
          width: 250.0,
          ),
          ),

          

              new Padding(
                padding: const EdgeInsets.all(8.0),
                child: new Text(_quotes,
                style: new TextStyle(
                  fontSize: 17.0,
                  letterSpacing: 1.1,
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.w300
                ),),
              ),

              new FlatButton.icon(icon: new Icon(Icons.navigate_next,),label: new Text('Read More'),onPressed: (){_launchbio(_biourl);},)
            ],
          )
        ],
      ),
    ),
);
}

static bool _lang = false;
_changeLanguage(bool lang){
  /*
  True  : Hindi
  False : English 
  */

  setState(() {
    _lang = lang;      
    });
}

  Widget _body(){

    return new Container(
      padding: new EdgeInsets.all(8.0),
      child: new Center(
        child: new Column(
          children: <Widget>[

            new Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                new Text('Hindi', style: new TextStyle(
                  fontSize: 17.0,
                  color: Colors.grey
                ),),
                new Switch(value: _lang, activeColor: Colors.blue, onChanged: (bool value){_changeLanguage(value);} ,)
              ],
            ),

            new Flexible(
                child: new ListView.builder(
                    padding: new EdgeInsets.all(8.0),
                    physics: const BouncingScrollPhysics(),
                    itemCount: _data == null ? 0 : _data.length,
                    scrollDirection: Axis.vertical,
                    itemBuilder: (_,int index){
                    return  _listitems(index);
                    },
                  ),
            ),
          ],
        ),
      ),
    );
  }
}