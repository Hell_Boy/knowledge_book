import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:knowledgebook/dataholder/tempdata.dart';

class Searchbook extends StatefulWidget{

  @override
  _State createState()=> new _State();
}

class _State extends State<Searchbook>{

  @override
  Widget build (BuildContext context){

    return new Scaffold(
      
      appBar: new AppBar(title: _searchbar()),
      body: new ListView.builder(
        itemCount: _listdata==null ? 0 : _listdata.length,
        padding: new EdgeInsets.all(5.0),
        itemBuilder: (_,int index){
          return _listitems(index);
        },
      ),
    );
  }

  Widget _searchbar(){

    return new TextField(
                autocorrect: true,
                maxLines: 1,
                onChanged: (String value){_listentextchange(value);},
                style: new TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                  letterSpacing: 1.5
                ),
                 decoration: new InputDecoration(
                  border: new UnderlineInputBorder(borderSide: BorderSide.none),
                  hintText: 'Enter Search text'
                ),
            );
  }

  _listentextchange(String value) async {
    var _url = "http://catchforms.in/knowledgebook/scripts/search.php";

    // For Onetime connection
    // http.post(_url, body: JSON.encode({"keyword": "$value"})).then((response){
    //   print(response.body);
    // });
    
    // For Conitnuous connection
    var client = http.Client();

    await client.post(_url,
    body: {"keyword": "$value"})
  .then((response) => _decodeResponse(response))
  .whenComplete(client.close);
  }

  List _listdata;
  _decodeResponse(http.Response response){
    
           Map _data = JSON.decode(response.body);
        
          if(_data["conn"]==1){

            setState(() {
                     _listdata = _data["data"] as List;
                                  
                });
               
       }else{
             _listdata = null;
      }
  }

  Widget _listitems(int index){

Map _indexdata = _listdata[index];
String _name = _indexdata["name"];
String _quotes = _indexdata["quotes"];
String _image = _indexdata["pictureurl"];
String _biourl= _indexdata["biographyurl"];

  return new Card(
    child: new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Column(
        children: <Widget>[

          new Column(
            children: <Widget>[
              
              new Center(
                child: new Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: new Text(_name,
                  style: new TextStyle(
                    fontSize: 20.0,
                    letterSpacing: 1.0,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.italic
                  ),),
                ),
              ),
          new Center(
          child: new Image.network(_image,
          height: 130.0,
          width: 250.0,
          ),
          ),

          

              new Padding(
                padding: const EdgeInsets.all(8.0),
                child: new Text(_quotes,
                style: new TextStyle(
                  fontSize: 17.0,
                  letterSpacing: 1.1,
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.w300
                ),),
              ),

              new FlatButton.icon(icon: new Icon(Icons.navigate_next,),label: new Text('Read More'),onPressed: (){_launchbio(_biourl);},)
            ],
          )
        ],
      ),
    ),
);
}

Future _launchbio(String url) async{
  final temp = new tempdata();
  temp.url=url;
  String _url1 = temp.url;
  Navigator.of(context).pushNamed('Bio');
  
}

}