import 'package:flutter/material.dart';
import 'dart:async';
import 'package:knowledgebook/dataholder/tempdata.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:knowledgebook/dataholder/tempdata.dart';

class Bio extends StatefulWidget{

@override
_State createState()=> new _State();
}

class _State extends State<Bio>{

String _url;

@override
void initState(){
  _url = new tempdata().url;
  
}

  @override
  Widget build(BuildContext context){
    
    return new WebviewScaffold(
      url: _url,
      withJavascript: true,
    );
  }
}
